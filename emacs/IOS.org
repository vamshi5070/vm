* IO 
*** IO miniframe
#+begin_src emacs-lisp
(straight-use-package 'mini-frame)
#+end_src
*** IO mini buffer header
#+begin_src emacs-lisp
(straight-use-package 'minibuffer-header)
#+end_src

*** IO Corfu, cape
#+begin_src emacs-lisp

  ;;;; Code Completion
  (use-package corfu
    :straight t
    ;; Optional customizations
    :custom
    (corfu-cycle t)                  ; Allows cycling through candidates
    (corfu-auto t)                   ; Enable auto compfletion
    (corfu-auto-prefix 2)            ; Enable auto completion
    (corfu-auto-delay 0.0)           ; Enable auto completion
    (corfu-quit-at-boundary 'separator)
    (corfu-echo-documentation 0.25)   ; Enable auto completion
    (corfu-preview-current 'insert)   ; Do not preview current candidate
    (corfu-preselect-first nil)

    ;; Optionally use TAB for cycling, default is `corfu-complete'.
    :bind (:map corfu-map
		("M-SPC" . corfu-insert-separator)
		("TAB"     . corfu-next)
		([tab]     . corfu-next)
		("S-TAB"   . corfu-previous)
		([backtab] . corfu-previous)
		("S-<return>" . corfu-insert)
		("RET"     . nil) ;; leave my enter alone!
		)

    :init
    (global-corfu-mode)
    ;; (corfu-history-mode)
    :config
    (add-hook 'eshell-mode-hook
	      (lambda () (setq-local corfu-quit-at-boundary t
				corfu-quit-no-match t
				corfu-auto nil)
		(corfu-mode))))

  ;; Add extensions
  (use-package cape
    :straight t
    :defer 10
    :init
    ;; Add `completion-at-point-functions', used by `completion-at-point'.
    (add-to-list 'completion-at-point-functions #'cape-file)
    (add-to-list 'completion-at-point-functions #'cape-dabbrev)
    :bind (("C-c p p" . completion-at-point) ;; capf
	 ("C-c p t" . complete-tag)        ;; etags
	 ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
	 ("C-c p h" . cape-history)
	 ("C-c p f" . cape-file)
	 ("C-c p k" . cape-keyword)
	 ("C-c p s" . cape-symbol)
	 ("C-c p a" . cape-abbrev)
	 ("C-c p i" . cape-ispell)
	 ("C-c p l" . cape-line)
	 ("C-c p w" . cape-dict)
	 ("C-c p \\" . cape-tex)
	 ("C-c p _" . cape-tex)
	 ("C-c p ^" . cape-tex)
	 ("C-c p &" . cape-sgml)
	 ("C-c p r" . cape-rfc1345))
    :config
    ;; Silence then pcomplete capf, no errors or messages!
    (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)

    ;; Ensure that pcomplete does not write to the buffer
    ;; and behaves as a pure `completion-at-point-function'.
    (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify))
#+end_src

* Tweaked Appearance
*** Modus theme
#+begin_src emacs-lisp
	(setq modus-themes-org-blocks nil
	      modus-themes-org-blocks 'grayscale
	      modus-themes-fringes 'nil
	      modus-themes-variable-pitch-ui t
	      modus-themes-prompts '(bold background)
	      modus-themes-completions 'opinionated
	      modus-themes-subtle-line-numbers t
	      modus-themes-italic-constructs t
	      modus-themes-syntax '(faint alt-syntax )
	      modus-themes-variable-pitch-headings t
	      modus-themes-variable-ui-headings t
	      modus-themes-subtle-line-numbers t
	      modus-themes-tabs-accented t
	      modus-themes-paren-match '(bold intense)
	      modus-themes-bold-constructs nil
	      ;; modus-themes-hl-line '(accented intense)
	      ;; modus-themes-region '(intense)
	      ;; modus-themes-mode-line '(accented 3d borderless (padding. 22) (height . 0.9))
	      modus-themes-org-agenda
	      '((header-block . (variable-pitch scale-title))
		(header-date . (bold-today grayscale scale))
		(scheduled . rainbow)
		(habit . traffic-light-deuteranopia))
      modus-themes-headings
	  '((1 . (background overline variable-pitch 1.28))
	    (2 . (variable-pitch 1.22))
	    (3 . (semibold 1.17))
	    (4 . (1.14))
	    (t . (monochrome)))
  )

(load-theme 'modus-vivendi t)

#+end_src
*** Modeline
#+begin_src emacs-lisp

  (defun hide-modeline( )
    (interactive)
    (setq mode-line-format nil)
    )
  (add-hook 'vterm-mode-hook #'hide-modeline)
  (add-hook 'shell-mode-hook #'hide-modeline)
  (add-hook 'haskell-interactive-mode-hook  #'hide-modeline) 
  (add-hook 'help-mode-hook  #'hide-modeline) 
  (setq-default header-line-format mode-line-format)

  (setq-default mode-line-format nil)

#+end_src

* Completion (Fake ido)
#+begin_src emacs-lisp
  ;; (fido-mode t)
  ;; (fido-vertical-mode t)
  ;; (setq completions-format 'one-column)
  ;; (ido-mode t)
#+end_src

* Opening emacs
#+begin_src emacs-lisp
(recentf-open-files)
#+end_src



