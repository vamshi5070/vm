#+title:      what is koka
#+date:       [2022-10-07 Fri 19:04]
#+filetags:   :planguage:
#+identifier: 20221007T190438

A strongly type functional-style language with effect types and handlers.
