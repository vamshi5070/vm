(defvar-local emux-modeline-state nil)

(defun emux-window-state ()
  (setq emux-modeline-state 
     (cond  (buffer-read-only "lock"))
  )
)
			  

(setq-default mode-line-format (list "%m " ))

(provide 'emux-modeline)
