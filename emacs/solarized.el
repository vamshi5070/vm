    (require 'solarized-theme)
      ;; (load-theme 'solarized-wombat-dark t)
      ;; (load-theme 'tao-yin t)

      ;; inspired vim's jellybeans color-theme

      (solarized-create-theme-file-with-palette 'light 'solarized-jellybeans-light
	'("#202020" "#ffffff"
	  "#ffb964" "#8fbfdc" "#a04040" "#b05080" "#805090" "#fad08a" "#99ad6a" "#8fbfdc"))


      (solarized-create-theme-file-with-palette 'dark 'solarized-jellybeans-dark
	'("#202020" "#ffffff"
	  "#ffb964" "#8fbfdc" "#a04040" "#b05080" "#805090" "#fad08a" "#99ad6a" "#8fbfdc"))
    ;; inspired emacs's mesa color-theme
    (solarized-create-theme-file-with-palette 'light 'solarized-mesa-light
      '("#000000" "#faf5ee"
	"#3388dd" "#ac3d1a" 
	"#dd2222" "#8b008b"
	"#00b7f0" "#1388a2"
	"#104e8b" "#00688b"))

    ;; inspired emacs's mesa color-theme
    (solarized-create-theme-file-with-palette 'dark 'solarized-mesa-dark
      '("#000000" "#faf5ee"
	"#3388dd" "#ac3d1a" 
	"#dd2222" "#8b008b"
	"#00b7f0" "#1388a2"
	"#104e8b" "#00688b"))
  ;; inspired emacs's solarized color-theme
  (solarized-create-theme-file-with-palette 'light 'solarized-solarized-light
    '("#002b36" "#fdf6e3"
      "#b58900" "#cb4b16" 
      "#dc322f" "#d33682" 
      "#6c71c4" "#268bd2" 
      "#2aa198" "#859900"))

  ;; inspired emacs's solarized color-theme
  (solarized-create-theme-file-with-palette 'dark 'solarized-solarized-dark
    '("#002b36" "#fdf6e3"
      "#b58900" "#cb4b16" 
      "#dc322f" "#d33682" 
      "#6c71c4" "#268bd2" 
      "#2aa198" "#859900"))

  ;; (load-theme 'solarized-solarized-light t)
  ;; (load-theme 'solarized-mesa-dark t)
   ;; (load-theme 'nord t)
   ;; (load-theme 'solarized-mesa-light t)
   ;; (load-theme 'solarized-mesa-light t)
    ;; (load-theme 'solarized-jellybeans-light t)
      ;; (load-theme 'doom-one t)
       ;; (load-theme 'solarized-zenburn t)

    ;; make the fringe stand out from the background
    (setq solarized-distinct-fringe-background t)

    ;; Don't change the font for some headings and titles
    ;; (setq solarized-use-variable-pitch nil)

    ;; make the modeline high contrast
    (setq solarized-high-contrast-mode-line t)
