{ config, pkgs, ... }: {
  programs.emacs = {
    enable = true;
    package =  pkgs.emacs; #pkgs.emacsNativeComp; # pkgs.emacsPgtkGcc;
    extraPackages = epkgs:
    with epkgs; [
      # editor
      # evil
      # evil-collection
      # improved org
      #  org
      hyperbole
      pdf-tools
      # vterm
      vterm
      #   multi-vterm
      # flycheck
      # dante
      #    org-journal
      #bling
      pulsar
      # lang
      nix-mode
      rust-mode
      rustic
      ormolu
      haskell-mode
      clojure-mode
      cider
      corfu
      lsp-mode
      rainbow-delimiters
      agda2-mode
      which-key
      agda-input
      all-the-icons
      # prot
      #     fontaine
      # denote
      # hydra (repitition)
      #      hydra
      # git
      magit
      envrc
    ];
  };
  services.emacs = {
    enable = true;
    client = {
      enable = true;
      arguments = [ "-c" ];
    };
    defaultEditor = true;
    socketActivation.enable = true;
  };

  home.file.".emacs.d/init.el".source = ./init.el;
  home.file.".emacs.d/early-init.el".source = ./early-init.el;
  home.packages = with pkgs; [ emacs-all-the-icons-fonts ripgrep fd ];
}
