(defun reload ()
 (interactive )
(load-file (expand-file-name "~/.emacs.d/init.el")))

(defun private-config ()
      (interactive )
(find-file (expand-file-name "~/vm/emacs/config.org")))
 ;; (require 'emux-minibuffer)

(defun homeUp ()
  (interactive) 
 (async-shell-command " home-manager switch --flake \"/home/vamshi/vm#nixos\""))

(setq org-ellipsis " ⤵ " ;;▼ 
      ;; org-startup-indented t
      org-src-tab-acts-natively t
      org-hide-emphasis-markers t
      org-fontify-done-headline t
      org-hide-leading-stars t
      org-pretty-entities t
      org-odd-levels-only t
      ) 
(add-hook 'org-mode-hook #'org-shifttab)

;; (add-hook 'org-mode-hook #'variable-pitch-mode)
;; (setq org-bullets-bullet-list '(
;; (add-hook 'org-mode-hook (lambda () (local-set-key (kbd "

(global-set-key (kbd "C-z" ) #'(lambda () (message "pressed C-z")))

(column-number-mode 1)

(global-display-line-numbers-mode 1)
     (setq display-line-numbers-type 'relative)
     (dolist (mode '( org-mode-hook
		       prog-mode-hook
		       ;; term-mode-hook
		       ;; vterm-mode-hook
		       ;; shell-mode-hook
		       ;; dired-mode-hook
		       ;; eshell-mode-hook
		       ))
 (add-hook mode (lambda () (display-line-numbers-mode 1))))

(add-to-list 'load-path "~/vm/emacs")

(fset 'yes-or-no-p 'y-or-n-p)

(blink-cursor-mode 0)
;; Vertical window divider
;; (setq window-divider-default-right-width 24)
;; (setq window-divider-default-places 'right-only)
;; (window-divider-mode 1)

(setq default-frame-alist
      (append (list
	       '(min-height . 1)
	       '(height     . 45)
	       '(min-width  . 1)
	       '(width      . 81)
	       '(vertical-scroll-bars . nil)
	       '(internal-border-width . 24)
	       '(left-fringe    . 1)
	       '(right-fringe   . 1)
	       '(tool-bar-lines . 0)
	       '(menu-bar-lines . 0))))

;; make the fringe stand out from the background
(setq solarized-distinct-fringe-background t)

;; Don't change the font for some headings and titles
;; (setq solarized-use-variable-pitch nil)

;; make the modeline high contrast
(setq solarized-high-contrast-mode-line t)

(set-face-attribute 'default nil
		    :font "SF Mono"
		    :height 44
		    :weight 'bold)

(set-face-attribute 'variable-pitch nil
		    :font "SauceCode Pro Nerd FOnt"
		    :height 40
		    :weight 'regular)
(set-face-attribute 'fixed-pitch nil
		    :font "SauceCode Pro Nerd FOnt"
		    :height 40
		    :weight 'regular)

 (setq modus-themes-italic-constructs t
	    modus-themes-syntax '(faint)
       modus-themes-completions 'opinionated)
;;	    modus-themes-bold-constructs nil
;; modus-themes-variable-pitch-ui t
;;	   modus-themes-region '(bg-only no-extend)
;;	    modus-themes-hl-line '(accented) 
	    ;; modus-themes-syntax '(yellow-comments)
	    ;; modus-themes-mode-line '(accented borderless))
;; Color modeline in active window, remove border
;;   (setq modus-themes-headings ;; Makes org headings more colorful
;;	   '((t . (rainbow))))
;; (load-theme 'modus-operandi t)

(global-hl-line-mode 1)
(setq x-underline-at-descent-line t)
(setq split-height-threshold nil)
(setq split-width-threshold 0)

;; (setq-default mode-line-format 
;; 		   (list  "  " mode-line-modified "  | %m:" 
;;  mode-line-buffer-identification " |"  " %l ," " %C |"  ))
;;    (setq-default header-line-format mode-line-format)
;;    (setq-default mode-line-format nil)
   ;; (set-face-background 'modeline "purple")

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
	auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

;; Save history
(savehist-mode t)
;; saveplace
(save-place-mode 1)                           ;; Remember point in files
(setq save-place-ignore-files-regexp  ;; Modified to add /tmp/* files
      (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				save-place-ignore-files-regexp t t))
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(global-visual-line-mode t)
  ;; (require 'emux-minibuffer)

(require 'dired)

      (put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
 (add-hook 'dired-mode-hook 'dired-hide-details-mode)
 (add-hook 'dired-mode-hook 'dired-omit-mode)
 (setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
            dired-recursive-deletes 'top  ;; Always ask recursive delete
            dired-dwim-target t	    ;; Copy in split mode with p
            dired-auto-revert-buffer t
            dired-listing-switches "-alh -agho --group-directories-first"
            dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
            dired-isearch-filenames 'dwim ;;)
            dired-omit-files "^\\.[^.].*"
            dired-omit-verbose nil
            dired-hide-details-hide-symlink-targets nil
            delete-by-moving-to-trash t
            )
(define-key dired-mode-map (kbd "j") 'dired-next-line)
     (define-key dired-mode-map (kbd "k") 'dired-previous-line)
     (define-key dired-mode-map (kbd "h") 'dired-up-directory)
     (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
      (define-key dired-mode-map (kbd "/") 'dired-goto-file)

(defun emux/horizontal-split ()
   (interactive )
   (split-window-below)
   (other-window 1))

(defun emux/vertical-split ()
   (interactive )
   (split-window-right)
   (other-window 1))

(defun emux/up-window ()
   (interactive )
   (other-window -1))
(defun emux/down-window ()
   (interactive )
   (other-window 1))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(setq package-list
      '(
	;; cape                ; Completion At Point Extensions
	orderless           ; Completion style for matching regexps in any order
	vertico             ; VERTical Interactive COmpletion
	;; marginalia          ; Enrich existing commands with completion annotations
	consult             ; Consulting completing-read
	;; corfu               ; Completion Overlay Region FUnction
	;; deft                ; Quickly browse, filter, and edit plain text notes
	;; elfeed              ; Emacs Atom/RSS feed reader
	;; elfeed-org          ; Configure elfeed with one or more org-mode files
	;; citar               ; Citation-related commands for org, latex, markdown
	;; citeproc            ; A CSL 1.0.2 Citation Processor
	;; flyspell-correct-popup ; Correcting words with flyspell via popup interface
	;; flyspell-popup      ; Correcting words with Flyspell in popup menus
	;; guess-language      ; Robust automatic language detection
	;; helpful             ; A better help buffer
	doom-themes
	solaire-mode
	doom-modeline
	haskell-mode
	nix-mode
	rust-mode
	nord-theme
	;; modus-themes
	;; highlight-indent-guides
	;; cider
	;; clojure-mode
	;; evil
	;; evil-collection
	;; general
	;; undo-tree
	;; org
	;; org-babel
	org-auto-tangle
	;; org-bullets 
	;; org-superstar
	solarized-theme ;; nice themes
	tao-theme
	;; doom-modeline
	ace-window
	corfu
	cape	
	magit
	mini-frame          ; Show minibuffer in child frame on read-from-minibuffer
	vertico-posframe
	envrc
	yasnippet
	; envrc
	;; imenu-list          ; Show imenu entries in a separate buffer
	;; magit               ; A Git porcelain inside Emacs.
	;; markdown-mode       ; Major mode for Markdown-formatted text
	;; multi-term          ; Managing multiple terminal buffers in Emacs.
	;; pinentry            ; GnuPG Pinentry server implementation
	;; use-package         ; A configuration macro for simplifying your .emacs
	;; which-key
    ))         ; Display available keybindings in popup

;; Install packages that are not yet installed
(dolist (package package-list)
  (straight-use-package package))

(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
                (setq  haskell-interactive-popup-errors nil)

(vertico-mode)
           (setq vertico-count 9
           vertico-resize nil
                 vertico-cycle t
                 completion-in-region-function
       (lambda (&rest args)
         (apply (if vertico-mode
                    #'consult-completion-in-region
                  #'completion--in-region)
                args)))

       (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
       (define-key vertico-map (kbd "C-j") #'vertico-next)
       (define-key vertico-map (kbd "C-k") #'vertico-previous)
       (define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
 ;; (define-key vertico-map (kbd "<backspace>") #'vertico-directory-delete-char)

       ;; (define-key vertico-map (kbd "M-h") #'rational-completion/minibuffer-backward-kill)

      (setq completion-styles '(orderless)
            completion-category-defaults nil
            completion-category-overrides '((file (styles partial-completion))))

   (global-set-key (kbd "C-x b") 'consult-buffer)
(vertico-posframe-mode 1)
;; (mini-frame-mode 1)
  ;; (require 'io-bling)
 ;; (require 'emux-minibuffer)

(require 'solarized-theme)
    (load-theme 'modus-vivendi t)
    ;; (load-theme 'solarized-wombat-dark t)
    ;; (load-theme 'tao-yin t)

    ;; inspired vim's jellybeans color-theme

    (solarized-create-theme-file-with-palette 'light 'solarized-jellybeans-light
      '("#202020" "#ffffff"
	"#ffb964" "#8fbfdc" "#a04040" "#b05080" "#805090" "#fad08a" "#99ad6a" "#8fbfdc"))


    (solarized-create-theme-file-with-palette 'dark 'solarized-jellybeans-dark
      '("#202020" "#ffffff"
	"#ffb964" "#8fbfdc" "#a04040" "#b05080" "#805090" "#fad08a" "#99ad6a" "#8fbfdc"))
  ;; inspired emacs's mesa color-theme
  (solarized-create-theme-file-with-palette 'light 'solarized-mesa-light
    '("#000000" "#faf5ee"
      "#3388dd" "#ac3d1a" 
      "#dd2222" "#8b008b"
      "#00b7f0" "#1388a2"
      "#104e8b" "#00688b"))

  ;; inspired emacs's mesa color-theme
  (solarized-create-theme-file-with-palette 'dark 'solarized-mesa-dark
    '("#000000" "#faf5ee"
      "#3388dd" "#ac3d1a" 
      "#dd2222" "#8b008b"
      "#00b7f0" "#1388a2"
      "#104e8b" "#00688b"))
;; inspired emacs's solarized color-theme
(solarized-create-theme-file-with-palette 'light 'solarized-solarized-light
  '("#002b36" "#fdf6e3"
    "#b58900" "#cb4b16" 
    "#dc322f" "#d33682" 
    "#6c71c4" "#268bd2" 
    "#2aa198" "#859900"))

;; inspired emacs's solarized color-theme
(solarized-create-theme-file-with-palette 'dark 'solarized-solarized-dark
  '("#002b36" "#fdf6e3"
    "#b58900" "#cb4b16" 
    "#dc322f" "#d33682" 
    "#6c71c4" "#268bd2" 
    "#2aa198" "#859900"))


;; (load-theme 'solarized-solarized-light t)
;; (load-theme 'solarized-mesa-dark t)
 ;; (load-theme 'nord t)
 ;; (load-theme 'solarized-mesa-light t)
 ;; (load-theme 'solarized-mesa-light t)
  ;; (load-theme 'solarized-jellybeans-light t)
    ;; (load-theme 'doom-one t)
     ;; (load-theme 'solarized-zenburn t)

;; (load-theme 'solarized-wombat-dark t)
;; (doom-modeline-mode 1)
   ;; (setq-default header-line-format mode-line-format)
   ;; (setq-default mode-line-format nil)

(envrc-global-mode 1)

(require 'org-auto-tangle)
(add-hook 'org-mode-hook 'org-auto-tangle-mode)

(global-set-key (kbd "M-o") 'ace-window)

  ;;   (require 'io-window)
    ;; (require 'emux-minibuffer)

;; Enable Corfu completion UI
;; See the Corfu README for more configuration tips.
;; (use-package corfu
;;   :straight t
;;   :init
  (global-corfu-mode)
;; )
;; Add extensions
;; (use-package cape
;;   :straight t
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  ;; :bind (("C-c p p" . completion-at-point) ;; capf
  ;;        ("C-c p t" . complete-tag)        ;; etags
  ;;        ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
  ;;        ("C-c p h" . cape-history)
  ;;        ("C-c p f" . cape-file)
  ;;        ("C-c p k" . cape-keyword)
  ;;        ("C-c p s" . cape-symbol)
  ;;        ("C-c p a" . cape-abbrev)
  ;;        ("C-c p i" . cape-ispell)
  ;;        ("C-c p l" . cape-line)
  ;;        ("C-c p w" . cape-dict)
  ;;        ("C-c p \\" . cape-tex)
  ;;        ("C-c p _" . cape-tex)
  ;;        ("C-c p ^" . cape-tex)
  ;;        ("C-c p &" . cape-sgml)
  ;;        ("C-c p r" . cape-rfc1345))
  ;; :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
 ;; (global-key cape-mode-map (kbd "j") 'dired-next-line)

(global-set-key (kbd "C-c p l") 'cape-line)
(global-set-key (kbd "C-c p d") 'cape-dabbrev)

  ;;(add-to-list 'completion-at-point-functions #'cape-history)
  ;;(add-to-list 'completion-at-point-functions #'cape-keyword)
  ;;(add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;;(add-to-list 'completion-at-point-functions #'cape-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
;; )
;; (provide 'io-cape)
  ;;  (require 'io-cape)
      ;; (require 'emux-minibuffer)

;; (use-package yasnippet
  ;;     :straight t)
    (setq yas-snippet-dirs
          '("~/vm/emacs/snippets/"))
(yas-global-mode 1)

;;(global-undo-tree-mode)
;;(add-hook 'evil-local-mode-hook 'turn-on-undo-tree-mode) 
;; (setq evil-undo-system 'undo-tree)
;; (setq evil-want-keybinding nil)
;;  (evil-mode 1)
;;  (evil-collection-init)
(require 'emux-keybindings)

(defun insert-file-name ()
    "Insert the full path file name into the current buffer."
    (interactive)
    (insert (buffer-file-name (window-buffer (minibuffer-selected-window)))))

(defun ff ()
  "Prompt user to enter a file name, with completion and history support."
  (interactive)
  (message "String is %s" (read-file-name "Enter file name:")))
  

(defun ghcid-ff ()
  "Prompt user to enter a file name, with completion and history support."
  (interactive)
  (async-shell-command (concat "nix develop -c ghcid " (buffer-file-name)))) ;;(read-file-name "Enter file name:")))) 
  
(defun ghcid ()
    (interactive)
    (async-shell-command "nix develop -c ghcid " (insert-file-name )))
