*** Face attribute
#+begin_src emacs-lisp
  ;; (global-set-key (kbd "C-c t") #'(lambda () (interactive) (message (current-time-string))))

      (defun normal-font ()
	(interactive)
      (set-face-attribute 'variable-pitch nil
			  :font "Monospace"
			  :height 45
			  :weight 'regular)
      (set-face-attribute 'default nil
			  :family "Monospace"
			  :weight 'regular
			  :height 40)
      (set-face-attribute 'fixed-pitch nil
			  :family "Monospace"
			  :weight 'regular
			  :height 40)
    )
      (defun big-font ()
	(interactive)
      (set-face-attribute 'variable-pitch nil
			  :font "Monospace"
			  :height 90
			  :weight 'regular)
      (set-face-attribute 'default nil
			  :family "Monospace"
			  :weight 'regular
			  :height 90)
      (set-face-attribute 'fixed-pitch nil
			  :family "Monospace"
			  :weight 'regular
			  :height 90)
	)
    (normal-font)

#+end_src
