(set-face-attribute 'default nil
                    :family "Roboto Mono"
                    :weight 'light
                    :height 70)

(set-face-attribute 'bold nil
                    :family "Roboto Mono"
                    :weight 'regular)

(set-face-attribute 'italic nil
                    :family "Victor Mono"
                    :weight 'semilight
                    :slant 'italic)

(set-fontset-font t 'unicode
                    (font-spec :name "Inconsolata Light"
                               :size 16) nil)

(set-fontset-font t '(#xe000 . #xffdd)
                     (font-spec :name "RobotoMono Nerd Font"
                                :size 12) nil)

(add-to-list 'load-path "~/vm/emacs")

(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(envrc-global-mode t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

;; Enable Corfu completion UI
;; See the Corfu README for more configuration tips.
(use-package corfu
  :straight t
  :init
  (global-corfu-mode))

;; Add extensions
(use-package cape
  :straight t
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("C-c p p" . completion-at-point) ;; capf
         ("C-c p t" . complete-tag)        ;; etags
         ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c p h" . cape-history)
         ("C-c p f" . cape-file)
         ("C-c p k" . cape-keyword)
         ("C-c p s" . cape-symbol)
         ("C-c p a" . cape-abbrev)
         ("C-c p i" . cape-ispell)
         ("C-c p l" . cape-line)
         ("C-c p w" . cape-dict)
         ("C-c p \\" . cape-tex)
         ("C-c p _" . cape-tex)
         ("C-c p ^" . cape-tex)
         ("C-c p &" . cape-sgml)
         ("C-c p r" . cape-rfc1345))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  ;;(add-to-list 'completion-at-point-functions #'cape-history)
  ;;(add-to-list 'completion-at-point-functions #'cape-keyword)
  ;;(add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;;(add-to-list 'completion-at-point-functions #'cape-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
)

(use-package marginalia
  :ensure t
  :config
  (marginalia-mode))

(use-package embark
  :ensure t

  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; (straight-use-package 'haskell-mode)
(require 'ob-haskell)
(add-hook 'haskell-mode-hook  'interactive-haskell-mode)
(setq  haskell-interactive-popup-errors nil)
(add-hook 'haskell-mode-hook 
	  (lambda ()
	    ;; (define-key xah-fly-command-map (kbd "Z") 'save-buffer)
	    ))

;; (straight-use-package 'nix-mode)   
;; (add-hook 'nix-mode-hook 'nix-mode-format)

(defun nix-format-save ()
  (interactive)
  (nix-mode-format)
  (save-buffer)
  )
(add-hook 'nix-mode-hook 
	  (lambda ()
	    ;; (define-key xah-fly-command-map (kbd "SPC ;") 'nix-format-save)
	    ))

(straight-use-package 'agda2-mode)

(straight-use-package '(nano-theme :type git :host github
                                   :repo "rougier/nano-theme"))

;; SVG tags, progress bars & icons
(straight-use-package
 '(svg-lib :type git :host github :repo "rougier/svg-lib"))

;; Replace keywords with SVG tags
(straight-use-package
 '(svg-tag-mode :type git :host github :repo "rougier/svg-tag-mode"))

(require 'svg-lib)
(require 'svg-tag-mode)

;; NANO modeline
(straight-use-package
 '(nano-modeline :type git :host github :repo "rougier/nano-modeline"))

;; NANO header
(straight-use-package
 '(minibuffer-header :type git :host github :repo "rougier/minibuffer-header"))
(require 'nano-theme)
(nano-dark)
(nano-modeline-mode)

(require 'minibuffer-header)

(setq minibuffer-header-show-message t
      minibuffer-header-hide-prompt t
      minibuffer-header-default-message "")

(set-face-attribute 'minibuffer-header-face nil
                    :inherit 'nano-subtle
                    :extend t)
(set-face-attribute 'minibuffer-header-message-face nil
                    :inherit '(nano-subtle nano-faded)
                    :extend t)

(defun my/minibuffer-header-format (prompt)
  "Minibuffer header"
  
  (let* ((prompt (replace-regexp-in-string "[: \t]*$" "" prompt))
         (depth (minibuffer-depth))
         (prompt (cond ((string= prompt "M-x") "Extended command")
                       ((string= prompt "Function") "Help on function")
                       ((string= prompt "Callable") "Help on function or macro")
                       ((string= prompt "Variable") "Help on variable")
                       ((string= prompt "Command") "Help on command")
                       ((string= prompt "Eval") "Evaluate lisp expression")
                       (t prompt))))
    (concat
     (propertize (format " %d " depth)
                 'face `(:inherit (nano-salient-i nano-strong)
                         :extend t))
     (propertize " "
                 'face 'nano-subtle 'display `(raise ,nano-modeline-space-top))

     (propertize prompt
                 'face `(:inherit (nano-subtle nano-strong nano-salient)
                         :extend t))
     (propertize " "
                 'face 'nano-subtle 'display `(raise ,nano-modeline-space-bottom))
     (propertize "\n" 'face 'highlight)
     (propertize " " 'face 'highlight
                     'display `(raise ,nano-modeline-space-top))
     (propertize "︎︎" 'face '(:inherit (nano-salient nano-strong)))
     (propertize " " 'face 'highlight
                     'display `(raise ,nano-modeline-space-bottom)))))

(setq minibuffer-header-format #'my/minibuffer-header-format)

(minibuffer-header-mode)

(defun my/minibuffer-setup ()

  (set-window-margins nil 0 0)
  (set-fringe-style '(0 . 0))
  (cursor-intangible-mode t)
  (face-remap-add-relative 'default :inherit 'highlight))

(add-hook 'minibuffer-setup-hook #'my/minibuffer-setup)


;; Code from https://stackoverflow.com/questions/965263
(defun my/lookup-function (keymap func)
  (let ((all-bindings (where-is-internal (if (symbolp func)
                                             func
                                           (cl-first func))
                                         keymap))
        keys key-bindings)
    (dolist (binding all-bindings)
      (when (and (vectorp binding)
                 (integerp (aref binding 0)))
        (push binding key-bindings)))
    (push (mapconcat #'key-description key-bindings " or ") keys)
    (car keys)))

(defun my/minibuffer-show-last-command-setup ()
  (setq minibuffer-header-default-message
   (my/lookup-function (current-global-map) this-command)))

(add-hook 'minibuffer-setup-hook #'my/minibuffer-show-last-command-setup)

(defun my/minibuffer-show-last-command-exit ()
  (setq minibuffer-header-default-message ""))
(add-hook 'minibuffer-exit-hook #'my/minibuffer-show-last-command-exit)

(defun my/vertico--resize-window (height)
  "Resize active minibuffer window to HEIGHT."
;;  (setq-local truncate-lines (< (point) (* 0.8 (vertico--window-width)))
    (setq-local truncate-lines t
                resize-mini-windows 'grow-only
                max-mini-window-height 1.0)
  (unless (frame-root-window-p (active-minibuffer-window))
    (unless vertico-resize
      (setq height (max height vertico-count)))
    (let* ((window-resize-pixelwise t)
           (dp (- (max (cdr (window-text-pixel-size))
                       (* (default-line-height) (1+ height)))
                  (window-pixel-height))))
      (when (or (and (> dp 0) (/= height 0))
                (and (< dp 0) (eq vertico-resize t)))
        (window-resize nil dp nil nil 'pixelwise)))))

(advice-add #'vertico--resize-window :override #'my/vertico--resize-window)


(setq minibuffer-prompt-properties '(read-only t
                                     cursor-intangible t
                                     face minibuffer-prompt)
      enable-recursive-minibuffers t)

(straight-use-package '( vertico :files (:defaults "extensions/*")
             :includes (vertico-buffer
                    vertico-directory
                    vertico-flat
                    vertico-indexed
                    vertico-mouse
                    vertico-quick
                    vertico-repeat
                    vertico-reverse)))
  ;; (straight-use-package 'vertico)
  (straight-use-package 'orderless)
    ;; (straight-use-package 'vertico-directory)
  (straight-use-package 'consult)
(setq consult-preview-key nil) ; No live preview
    (vertico-mode t)
    (vertico-indexed-mode t)
           (setq vertico-count 17
           vertico-resize nil
             vertico-cycle t
             vertico-count-format nil ; No prefix with number of entries
             completion-in-region-function
       (lambda (&rest args)
         (apply (if vertico-mode
                #'consult-completion-in-region
              #'completion--in-region)
            args)))

       (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
       (define-key vertico-map (kbd "C-j") #'vertico-next)
       (define-key vertico-map (kbd "C-k") #'vertico-previous)
       (define-key vertico-map (kbd "C-'") #'vertico-quick-insert)
       (define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
     ;; (define-key vertico-map (kbd "<backspace>") #'vertico-directory-delete-char)
  ;; (require 'vertico-directory)

    ;; Configure directory extension.
    (use-package vertico-directory
  :straight nil
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
          ("RET" . vertico-directory-enter)
          ("DEL" . vertico-directory-delete-char)
          ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))
      (setq completion-styles '(orderless)
            completion-category-defaults nil
            completion-category-overrides '((file (styles partial-completion))))

       (global-set-key (kbd "C-x b") 'consult-buffer)
       (global-set-key (kbd "C-x C-b") 'consult-buffer)
    ;; (vertico-posframe-mode 1)
    ;; (mini-frame-mode 1)	;; 
    ;; (minibuffer-header-mode 1)
      ;; (require 'io-bling)
     ;; (require 'emux-minibuffer)


    ;; (load-library 'ob-haskell nil)

(setq vertico-grid-separator
      #("  |  " 2 3 (display (space :width (1))
                             face (:background "#ECEFF1")))

      vertico-group-format
      (concat #(" " 0 1 (face vertico-group-title))
              #(" " 0 1 (face vertico-group-separator))
              #(" %s " 0 4 (face vertico-group-title))
              #(" " 0 1 (face vertico-group-separator
                          display (space :align-to (- right (-1 . right-margin) (- +1)))))))

(set-face-attribute 'vertico-group-separator nil
                    :strike-through t)
(set-face-attribute 'vertico-current nil
                    :inherit '(nano-strong nano-subtle))
(set-face-attribute 'completions-first-difference nil
                    :inherit '(nano-default))

(defun minibuffer-format-candidate (orig cand prefix suffix index _start)
  (let ((prefix (if (= vertico--index index)
                    "  "
                  "   "))) 
    (funcall orig cand prefix suffix index _start)))

(advice-add #'vertico--format-candidate
           :around #'minibuffer-format-candidate)

(defun vertico--prompt-selection ()
  "Highlight the prompt"

  (let ((inhibit-modification-hooks t))
    (set-text-properties (minibuffer-prompt-end) (point-max)
                         '(face (nano-strong nano-salient)))))
 
(defun minibuffer-vertico-setup ()

  (setq truncate-lines t)
  (setq completion-in-region-function
        (if vertico-mode
            #'consult-completion-in-region
          #'completion--in-region)))

(add-hook 'vertico-mode-hook #'minibuffer-vertico-setup)
(add-hook 'minibuffer-setup-hook #'minibuffer-vertico-setup)

;; (straight-use-package 'pulsar)
(customize-set-variable
 'pulsar-pulse-functions
 '(other-window
   xah-next-window-or-frame)
 )
(setq pulsar-face 'pulsar-magenta)
(setq pulsar-delay 0.055)
;; (define-key global-map (kbd  
(pulsar-global-mode t)
(global-hl-line-mode t)

(use-package hyperbole
  :straight t
  :config (require 'hyperbole)
  :bind* ("<M-return>" . hkey-either))

(defun iso-func ()
  "Next paragraph as an isolated function, valid for only haskell"
  (interactive )
  (progn
    (imenu (imenu-choose-buffer-index ))
    (let ((strat (point)))
      (forward-paragraph)
      (narrow-to-region strat (point)))))
(global-set-key (kbd "C-c f") 'iso-func)

(use-package meow
  :straight t)
  ;; :config (require 'hyperbole)
  ;; :bind* ("<M-return>" . hkey-either))

(defun meow-setup ()
  (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
  (meow-motion-overwrite-define-key
   '("j" . meow-next)
   '("k" . meow-prev)
   '("<escape>" . ignore))
  (meow-leader-define-key
   ;; SPC j/k will run the original command in MOTION state.
   '("j" . "H-j")
   '("k" . "H-k")
   ;; Use SPC (0-9) for digit arguments.
   '("ss" . consult-line)
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   '("0" . meow-expand-0)
   '("9" . meow-expand-9)
   '("8" . meow-expand-8)
   '("7" . meow-expand-7)
   '("6" . meow-expand-6)
   '("5" . meow-expand-5)
   '("4" . meow-expand-4)
   '("3" . meow-expand-3)
   '("2" . meow-expand-2)
   '("1" . meow-expand-1)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("[" . meow-beginning-of-thing)
   '("]" . meow-end-of-thing)
   '("a" . meow-append)
   '("A" . meow-open-below)
   '("b" . meow-back-word)
   '("B" . meow-back-symbol)
   '("c" . meow-change)
   '("d" . meow-delete)
   '("D" . meow-backward-delete)
   '("e" . meow-next-word)
   '("E" . meow-next-symbol)
   '("f" . meow-find)
   '("g" . meow-cancel-selection)
   '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-insert)
   '("I" . meow-open-above)
   '("j" . meow-next)
   '("J" . meow-next-expand)
   '("k" . meow-prev)
   '("K" . meow-prev-expand)
   '("l" . meow-right)
   '("L" . meow-right-expand)
   '("m" . meow-join)
   '("n" . meow-search)
   '("o" . meow-block)
   '("O" . meow-to-block)
   '("p" . meow-yank)
   '("q" . meow-quit)
   '("Q" . meow-goto-line)
   '("r" . meow-replace)
   '("R" . meow-swap-grab)
   '("s" . meow-kill)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . meow-undo-in-selection)
   '("v" . meow-visit)
   '("w" . meow-mark-word)
   '("W" . meow-mark-symbol)
   '("x" . meow-line)
   '("X" . meow-goto-line)
   '("y" . meow-save)
   '("Y" . meow-sync-grab)
   '("z" . meow-pop-selection)
   '("'" . repeat)
   '("<escape>" . ignore)))
(meow-setup)
(meow-global-mode t)

(use-package el-patch
  :straight t
  :custom
  (el-patch-enable-use-package-integration t))

(use-package orderless
  :straight t)
(setq completion-styles '(orderless)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion))))

(use-package consult
  :straight t)

(global-set-key (kbd "C-x b") 'consult-buffer)
;; (global-set-key (kbd "C-x b") 'consult-buffer)

(straight-use-package 'doom-modeline)
;; (doom-modeline-mode t)
(require 'all-the-icons)

(setq custom-file (concat "~/vm/emacs/" "custom.el"))

(when (file-exists-p custom-file)
  (load custom-file nil t))

;; (load-theme 'modus-vivendi t)

(column-number-mode 1)
(global-display-line-numbers-mode 0)
(setq display-line-numbers-type 'visual)
(dolist (mode '( org-mode-hook
		 prog-mode-hook
		 dired-mode-hook

		 ;; term-mode-hook
		 ;; vterm-mode-hook
		 ;; shell-mode-hook
		 ;; dired-mode-hook
		 ;; eshell-mode-hook
		 ))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(savehist-mode t)
(save-place-mode t)
(setq save-place-ignore-files-regexp
      (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				save-place-ignore-files-regexp t t))

(global-set-key (kbd "C-x C-o") 'other-window)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))
(setq create-lockfiles nil)

(setq 
 ;; org-ellipsis "▼  " ;;⤵ 
 ;; org-startup-indented t
 org-src-tab-acts-natively t
 org-hide-emphasis-markers t
 org-fontify-done-headline t
 org-hide-leading-stars t
 org-pretty-entities t
 org-odd-levels-only t
 ) 
;;  (add-hook 'org-mode-hook #'org-shifttab)
;; (add-hook 'org-mode-hook #'variable-pitch-mode)
;; (setq org-bullets-bullet-list '(
;; (add-hook 'org-mode-hook (lambda () (local-set-key (kbd "
(setq org-src-fontify-natively t
      org-startup-folded t
      org-edit-src-content-indentation 0)

(setq org-agenda-files '("~/tasks/timetable.org")
      org-agenda-span  'week
      org-todo-keywords '((sequence "NEXT(n)" "TODO(t)" "WAITING(w)" "SOMEDAY(s)" "|" "DONE(d)" "CANCELLED (c)")) 
      org-log-done 'time
      org-log-done 'note
      )

(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-log-into-drawer '("LOGBOOK"))

(visual-line-mode t)
(electric-pair-mode t)

(defun roboto-font ()
(interactive)
(set-face-attribute 'default nil :font "Roboto Mono Nerd Font " :height 70 :weight 'regular)
(set-face-attribute 'fixed-pitch nil :font "Roboto Mono Nerd Font" :height 70 :weight 'regular)
(set-face-attribute 'variable-pitch nil :font "Roboto Mono Nerd Font" :height 70 :weight 'regular)
)
(defun sf-mono-font ()
(interactive)
(set-face-attribute 'default nil :font "SF Mono " :height 110 :weight 'regular)
(set-face-attribute 'fixed-pitch nil :font "SF Mono" :height 110 :weight 'regular)
(set-face-attribute 'variable-pitch nil :font "SF Mono" :height 90 :weight 'regular)
)

(defun big-font ()
(interactive)
(set-face-attribute 'default nil :font "Victor mono " :height 100)
(set-face-attribute 'fixed-pitch nil :font "Victor mono" :height 100)
)
;; (add-hook p
;; (add-hook 'prog-mode-hook #'variable-pitch-mode)
;;(setq line-spacing 5)
;; (roboto-font)
;; (sf-mono-font)
;; (normal-font)

;; (

(defun org-tangle-save ()
  (interactive)
  (save-buffer)
  (org-babel-tangle)
  )

(add-hook 'org-mode-hook 
	  (lambda ()
	    ;; (define-key xah-fly-command-map (kbd "SPC ;") 'org-tangle-save)
	    ))

(defun reload ()
  (interactive )
  (load-file (expand-file-name "~/.emacs.d/init.el")))

(defun private-config ()
  "The core config of emacs"
  (interactive)
  (find-file (expand-file-name "~/vm/emacs/void.org")))

(defun emux/getBuffer()
  (interactive)
  (setq buff (read-buffer-to-switch "Raise window"))
  (display-buffer buff '(display-buffer-reuse-window . ())))

(defun emux/go-to-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Go to window"))
  (select-window (get-buffer-window buff)))

(defun emux/get-and-to-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Raise & Go to window"))
  (display-buffer buff '(display-buffer-reuse-window . ()))
  (select-window (get-buffer-window buff)))

(defun emux/close-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Close window"))
  (delete-window (get-buffer-window buff)))

(defun emux/close-help ()
  (interactive)
  (setq buff "*Help*") 
  (delete-window (get-buffer-window buff)))

(defun emux/close-scratch ()
  (interactive)
  (setq buff "*scratch*") 
  (delete-window (get-buffer-window buff)))

(require 'window)
(setq display-buffer-alist
      `(
	("\\*vterm*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . 0)
	 )
	("\\*shell*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . -1)
	 )
	("\\`\\*Async Shell Command\\*\\'"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . right)
	 (slot . 0)
	 )
	("\\*scratch*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top)
	 (slot . 0)
	 )
	;; ("*"
	;;  (display-buffer-in-side-window)
	;;  (mode . (dired-mode))
	;;  (dedicated . t)
	;;  (window-height . 0.23)
	;;  (side . bottom)
	;;  (slot . 0)
	;;  )
	("*Help*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom) 
	 (slot . 1)
	 )
	("*Occur*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top) 
	 (slot . -1)
	 )
	("*grep*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top) 
	 (slot . 1)
	 )
	("\\*haskell*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . -1)
	 )
	)
      )

(require 'dired)

(put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(add-hook 'dired-mode-hook 'dired-omit-mode)
(setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
	      dired-recursive-deletes 'top  ;; Always ask recursive delete
	      dired-dwim-target t	    ;; Copy in split mode with p
	      dired-auto-revert-buffer t
	      dired-listing-switches "-alh -agho --group-directories-first"
	      dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
	      dired-isearch-filenames 'dwim ;;)
	      dired-omit-files "^\\.[^.].*"
	      dired-omit-verbose nil
	      dired-hide-details-hide-symlink-targets nil
	      delete-by-moving-to-trash t
	      )
;; (emux/leader-key-def 
;; "fd" 'dired-jump)

(defun emux/window-front ()
  (interactive)
  (other-window 1))

(defun emux/window-back ()
  (interactive)
  (other-window -1))

(defun emux/horizontal-split ()
  (interactive )
  (split-window-below)
  (emux/window-front))
;; (other-window 1))

(defun emux/vertical-split ()
  (interactive )
  (split-window-right)
  (emux/window-front))
;;  (other-window 1))

(defun emux/up-window ()
  (interactive )
  (emux/window-back))
;;  (other-window -1))

(defun emux/down-window ()
  (interactive )
  (emux/window-front))
(defun emux/swap-with-front-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-front)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-back)
	(switch-to-buffer other-buff)))))
(defun emux/swap-with-back-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-back)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-front)
	(switch-to-buffer other-buff)))))
;;	   (other-window 1))

(defun next-block-org ()
  (interactive)
  (progn
    ;; org-next-visible-heading
    org-babel-next-src-block
    org-edit-src-code))

;; (def

(define-key minibuffer-local-must-match-map (kbd "<escape>") 'abort-minibuffers)
;; (require 'prog-keybindings)
;; (require 'org-keybindings)
;; (require 'dired-keybindings)

(add-hook 'Info-mode-hook 
	  (lambda ()
	    (define-key Info-mode-map "j" 'scroll-up-line)
	    (define-key Info-mode-map "k" 'scroll-down-line)
	    ))

;;#'variable-pitch-mode)

(global-set-key (kbd "C-c b") 'emux/get-and-to-window)
(global-set-key (kbd "C-c c b") 'emux/close-window)

(global-set-key "\C-x\C-m" 'execute-extended-command)
;; (global-set-key [remap dabbrev-expand] #'hippie-expand)

(global-set-key (kbd "C-z" ) #'(lambda () (interactive) (message "pressed C-z")))

(global-set-key (kbd "M-." ) #'(lambda () (interactive) (message "pressed M-.")))

(add-hook 'help-mode-hook 
	  (lambda ()
	    (define-key help-mode-map "j" 'scroll-up-line)
	    (define-key help-mode-map "k" 'scroll-down-line)
	    ))

(blink-cursor-mode 0)

(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "M-?") #'(lambda () (interactive) (hippie-expand -1)))
