    (define-minor-mode org-liger
			"Docstring"
		       ;; initial value
			:init-value
			nil
		       ;; indicator for mode line
			:lighter	   "org-liger"
			:keymap
			;; :after-hook  (message "rgv")
	       '(
				 ((kbd "i") . kill-word)
	       ((kbd "e") . previous-line)
				 ((kbd "k") . xah-delete-backward-char-or-bracket-text)
	       ((kbd "d") . next-line)
 ((kbd "p") . xah-reformat-lines)
 ((kbd "q") . xah-insert-space-before)
 ((kbd "o") . xah-shrink-whitespaces)
 ((kbd "r") . forward-word)
 ((kbd "u") . backward-kill-word)
 ((kbd "w") . backward-word)
((kbd "y") . set-mark-command)
((kbd "t") . undo)
((kbd "a") . xah-end-of-line-or-block)
((kbd "s") . forward-char)
((kbd "f") . xah-fly-insert-mode-activate)
((kbd "g") . xah-beginning-of-line-or-block)
((kbd "h") . xah-delete-current-text-block)
((kbd "j") . xah-fly-inser-mode-activate)
((kbd "k") . xah-delete-backward-char-or-bracket-text)
((kbd "l") . open-line)
((kbd "a") . xah-fly-M-x)
((kbd "z") . xah-goto-matching-bracket)
 ((kbd "x") . xah-forward-right-bracket)
 ((kbd "c") . xah-next-window-or-frame)
 ((kbd "v") . xah-backward-left-bracket)
 ((kbd "b") . isearch-forward)
  ((kbd "n") .  xah-toggle-letter-case)
   ((kbd "m") . xah-paste-or-paste-previous)
   ((kbd ",")  . xah-copy-line-or-region)
    ((kbd ".") . xah-cut-line-or-region)
     ((kbd "/") . xah-comment-dwim)
))  
(defun toggle-modal (&optional set-state)
  "Toggle `my-command-mode', optionally ensuring its state with `SET-STATE'.

			   `SET-STATE' is interpreted as follows:
			     nil   (Same as no argument) Toggle `my-command-mode'
			     -1    Ensure `my-command-mode' is disabled
			     else  Ensure `my-command-mode' is enabled
			   "
  (interactive )
  (cond ((equal set-state -1)
	 (when modal
	   (modal -1)))

	((equal set-state nil)
	 (modal (if modal -1 1)))

	(else
	 (unless modal
	   (modal 1)))))

(defun prog-modal-normal-mode ()
  (interactive)
  ;; (setq-default cursor-type '(hbar . 0 ))
  (setq cursor-type 'box) 
					; Set cursor color to white
  ;; (set-cursor-color "#ffffff") 
  ;; (message "Entered normal mode")
  (org-modal 1))

(defun prog-modal-insert-mode ()
  (interactive)
  ;;(set-cursor-color "blue")
  (setq cursor-type '(hbar .  2))
  ;; (setq-default cursor-type 'bar)
  ;; (message "Entered insert mode")
  (prog-modal -1))

(define-key prog-mode-map (kbd "C-a") 'prog-modal-normal-mode)
(define-key prog-mode-map (kbd "<escape>") 'prog-modal-normal-mode)
(add-hook 'prog-mode-hook #'prog-modal-normal-mode)
