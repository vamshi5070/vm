(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(straight-use-package
             '(nano-emacs :type git :host github :repo "rougier/nano-emacs"))
(set-face-attribute 'default t
                        :background "#000000"
                        :foreground "#ffffff"
                        :family "Roboto Mono Nerd FOnt"
                        :height 10)

    (setq nano-font-family-monospaced "iA writer duospace") ;; "Roboto Mono Nerd FOnt")
    (setq nano-font-size 4)
    (require 'nano-layout)

    (defun nano-theme-set-spaceduck ()
      (setq frame-background-mode 'dark)
      (setq nano-color-foreground "#ecf0c1")
      (setq nano-color-background "#0f111b")
      (setq nano-color-highlight  "#1b1c36")
      (setq nano-color-critical   "#e33400")
      (setq nano-color-salient    "#00a4cc")
      (setq nano-color-strong     "#eeeee3")
      (setq nano-color-popout     "#f2ce00")
      (setq nano-color-subtle     "#7a5ccc")
      (setq nano-color-faded      "#b3a1e6"))

    (nano-theme-set-spaceduck)

    (require 'nano-faces)
    (nano-faces)
    (require 'nano-theme)
   (nano-theme)
    (set-face-attribute 'bold nil :weight 'bold)
 ;;   (require 'nano-defaults)
  (require 'nano-session)

  (require 'nano-modeline)

(let ((inhibit-message t))
  (message "Welcome to GNU Emacs / N Λ N O edition")
  (message (format "Initialization time: %s" (emacs-init-time))))

(require 'nano-splash)

;;(provide 'emux-nano)

(add-to-list 'load-path "~/vm/emacs")

(prefer-coding-system 'utf-8)	        ;; Encoding
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

(global-set-key (kbd "C-z" ) #'(lambda () (message "pressed C-z")))

(defun reload ()
 (interactive )
(load-file (expand-file-name "~/.emacs.d/init.el")))

(defun private-config ()
      (interactive )
(find-file (expand-file-name "~/vm/emacs/config.org")))
 ;; (require 'emux-minibuffer)

(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (defalias 'yes-or-no-p 'y-or-n-p))

(column-number-mode 1)

(global-display-line-numbers-mode 1)
     (setq display-line-numbers-type 'relative)
     (dolist (mode '( org-mode-hook
		       prog-mode-hook
		       ;; term-mode-hook
		       ;; vterm-mode-hook
		       ;; shell-mode-hook
		       ;; dired-mode-hook
		       ;; eshell-mode-hook
		       ))
 (add-hook mode (lambda () (display-line-numbers-mode 1))))

(setq org-ellipsis " ▼ " ;; ⤵
      org-startup-indented t
      org-src-tab-acts-natively t
      org-hide-emphasis-markers t
      org-fontify-done-headline t
      org-hide-leading-stars t
      org-pretty-entities t
      org-odd-levels-only t
      ) 
(add-hook 'org-mode-hook #'org-shifttab)

;; (setq org-bullets-bullet-list '(
;; (add-hook 'org-mode-hook (lambda () (local-set-key (kbd "

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
	auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

;; Save history
(savehist-mode t)
;; saveplace
(save-place-mode 1)                           ;; Remember point in files
(setq save-place-ignore-files-regexp  ;; Modified to add /tmp/* files
      (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				save-place-ignore-files-regexp t t))
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(global-visual-line-mode t)
  ;; (require 'emux-minibuffer)

(require 'dired)

      (put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
 (add-hook 'dired-mode-hook 'dired-hide-details-mode)
 (add-hook 'dired-mode-hook 'dired-omit-mode)
 (setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
	    dired-recursive-deletes 'top  ;; Always ask recursive delete
	    dired-dwim-target t	    ;; Copy in split mode with p
	    dired-auto-revert-buffer t
	    dired-listing-switches "-alh -agho --group-directories-first"
	    dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
	    dired-isearch-filenames 'dwim ;;)
	    dired-omit-files "^\\.[^.].*"
	    dired-omit-verbose nil
	    dired-hide-details-hide-symlink-targets nil
	    delete-by-moving-to-trash t
	    )
(define-key dired-mode-map (kbd "j") 'dired-next-line)
     (define-key dired-mode-map (kbd "k") 'dired-previous-line)
     (define-key dired-mode-map (kbd "h") 'dired-up-directory)
     (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
      (define-key dired-mode-map (kbd "/") 'dired-goto-file)

(defun emux/horizontal-split ()
   (interactive )
   (split-window-below)
   (other-window 1))

(defun emux/vertical-split ()
   (interactive )
   (split-window-right)
   (other-window 1))

(defun emux/up-window ()
   (interactive )
   (other-window -1))
(defun emux/down-window ()
   (interactive )
   (other-window 1))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(setq package-list
      '(
	;; cape                ; Completion At Point Extensions
	orderless           ; Completion style for matching regexps in any order
	vertico             ; VERTical Interactive COmpletion
	;; marginalia          ; Enrich existing commands with completion annotations
	consult             ; Consulting completing-read
	;; corfu               ; Completion Overlay Region FUnction
	;; deft                ; Quickly browse, filter, and edit plain text notes
	;; elfeed              ; Emacs Atom/RSS feed reader
	;; elfeed-org          ; Configure elfeed with one or more org-mode files
	;; citar               ; Citation-related commands for org, latex, markdown
	;; citeproc            ; A CSL 1.0.2 Citation Processor
	;; flyspell-correct-popup ; Correcting words with flyspell via popup interface
	;; flyspell-popup      ; Correcting words with Flyspell in popup menus
	;; guess-language      ; Robust automatic language detection
	;; helpful             ; A better help buffer
	haskell-mode
	nix-mode
	;; rust-mode
	;; cider
	;; clojure-mode
	;; evil
	;; org
	;; org-babel
	mini-frame
	org-auto-tangle
	org-bullets 
	org-superstar
	solarized-theme ;; nice themes
	tao-theme
	;; doom-modeline
	ace-window
	corfu
	cape	
	magit
	;; mini-frame          ; Show minibuffer in child frame on read-from-minibuffer
	envrc 
	; envrc
	;; imenu-list          ; Show imenu entries in a separate buffer
	;; magit               ; A Git porcelain inside Emacs.
	;; markdown-mode       ; Major mode for Markdown-formatted text
	;; multi-term          ; Managing multiple terminal buffers in Emacs.
	;; pinentry            ; GnuPG Pinentry server implementation
	;; use-package         ; A configuration macro for simplifying your .emacs
	;; which-key
    ))         ; Display available keybindings in popup

;; Install packages that are not yet installed
(dolist (package package-list)
  (straight-use-package package))

(vertico-mode)
	   (setq vertico-count 17
           vertico-resize nil
	         vertico-cycle t
           completion-in-region-function
       (lambda (&rest args)
         (apply (if vertico-mode
                    #'consult-completion-in-region
                  #'completion--in-region)
                args)))

       (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
       (define-key vertico-map (kbd "C-j") #'vertico-next)
       (define-key vertico-map (kbd "C-k") #'vertico-previous)
       (define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
 ;; (define-key vertico-map (kbd "<backspace>") #'vertico-directory-delete-char)
       ;; (define-key vertico-map (kbd "M-h") #'rational-completion/minibuffer-backward-kill)

      (setq completion-styles '(orderless)
	    completion-category-defaults nil
	    completion-category-overrides '((file (styles partial-completion))))

   (global-set-key (kbd "C-x b") 'consult-buffer)
  ;; (require 'io-bling)
 ;; (require 'emux-minibuffer)

(require 'emux-keybindings)
