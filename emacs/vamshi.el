(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

;; (straight-use-package 'pulsar)
(customize-set-variable
 'pulsar-pulse-functions
 '(other-window
 xah-next-window-or-frame)
)
(setq pulsar-face 'pulsar-magenta)
(setq pulsar-delay 0.055)
;; (define-key global-map (kbd  
(pulsar-global-mode t)
(global-hl-line-mode t)

;; (setq fontaine-presets
;;             '((regular
;;                :default-height 30)
;;               (small
;;                :default-height 10)
;;               (large
;;                :default-weight semilight
;;                :default-height 120
;;                :bold-weight extrabold)
;;               (extra-large
;;                :default-weight semilight
;;                :default-height 150
;;                :bold-weight extrabold)
;;               (t                        ; our shared fallback properties
;;                :default-family "Source Code pro"
;;                :default-weight normal)))
;;       (fontaine-set-preset 'regular)

(straight-use-package 'denote)

(envrc-global-mode t)

(use-package el-patch
  :straight t
  :custom
  (el-patch-enable-use-package-integration t))

(use-package vertico
  :straight (:host github :repo "minad/vertico"
             :includes (vertico-repeat vertico-directory vertico-buffer)
             :files (:defaults "extensions/vertico-directory.el" "extensions/vertico-buffer.el" "extensions/vertico-repeat.el"))
  :bind (:map vertico-map
         ("<escape>" . #'minibuffer-keyboard-quit)
         ("C-n"      . #'vertico-next-group      )
         ("C-p"      . #'vertico-previous-group  )
         ("C-j"      . #'vertico-next            )
         ("C-k"      . #'vertico-previous        )
         ("M-RET"    . #'vertico-exit))
  :hook (emacs-startup . vertico-mode)
  :config
  ;; Cycle through candidates
  (setq vertico-cycle t)

  ;; Don't resize buffer
  (setq vertico-resize nil)

  ;; try the `completion-category-sort-function' first
  (advice-add #'vertico--sort-function :before-until #'completion-category-sort-function)

  (defun completion-category-sort-function ()
    (alist-get (vertico--metadata-get 'category)
               completion-category-sort-function-overrides))

  (defvar completion-category-sort-function-overrides
    '((file . directories-before-files))
    "Completion category-specific sorting function overrides.")

  (defun directories-before-files (files)
    ;; Still sort by history position, length and alphabetically
    (setq files (vertico-sort-history-length-alpha files))
    ;; But then move directories first
    (nconc (seq-filter (lambda (x) (string-suffix-p "/" x)) files)
           (seq-remove (lambda (x) (string-suffix-p "/" x)) files))))

;;;;; Vertico Packages
;; Use vertico in buffer
(use-package vertico-buffer
  :after vertico
  :custom
  (vertico-buffer-hide-prompt t)
  :config/el-patch
  ;; Use el-patch
  ;; Set no headerline in vertico-buffer
  (defun vertico-buffer--setup ()
    "Setup buffer display."
    (add-hook 'pre-redisplay-functions 'vertico-buffer--redisplay nil 'local)
    (let* ((action vertico-buffer-display-action) tmp win
           (_ (unwind-protect
                  (progn
                    (setq tmp (generate-new-buffer "*vertico*")
                          ;; Temporarily select the original window such
                          ;; that `display-buffer-same-window' works.
                          win (with-minibuffer-selected-window (display-buffer tmp action)))
                    (set-window-buffer win (current-buffer)))
                (kill-buffer tmp)))
           (sym (make-symbol "vertico-buffer--destroy"))
           (depth (recursion-depth))
           (now (window-parameter win 'no-other-window))
           (ndow (window-parameter win 'no-delete-other-windows)))
      (fset sym (lambda ()
                  (when (= depth (recursion-depth))
                    (with-selected-window (active-minibuffer-window)
                      (when (window-live-p win)
                        (set-window-parameter win 'no-other-window now)
                        (set-window-parameter win 'no-delete-other-windows ndow))
                      (when vertico-buffer-hide-prompt
                        (set-window-vscroll nil 0))
                      (remove-hook 'minibuffer-exit-hook sym)))))
      ;; NOTE: We cannot use a buffer-local minibuffer-exit-hook here.
      ;; The hook will not be called when abnormally exiting the minibuffer
      ;; from another buffer via `keyboard-escape-quit'.
      (add-hook 'minibuffer-exit-hook sym)
      (set-window-parameter win 'no-other-window t)
      (set-window-parameter win 'no-delete-other-windows t)
      (overlay-put vertico--candidates-ov 'window win)
      (when (and vertico-buffer-hide-prompt vertico--count-ov)
        (overlay-put vertico--count-ov 'window win))
      (setq-local show-trailing-whitespace nil
                  truncate-lines t
                  face-remapping-alist
                  (copy-tree `((mode-line-inactive mode-line)
                               ,@face-remapping-alist))
                  header-line-format nil
                  mode-line-format nil
                  cursor-in-non-selected-windows 'box
                  vertico-count (- (/ (window-pixel-height win)
                                      (default-line-height)) 2))))
  :config
  ;; put minibuffer at top -- this is the more natural place to be looking!
  (setq vertico-buffer-display-action
        '(display-buffer-in-side-window
          (window-height . 13)
          (side . top)))
  (vertico-buffer-mode 1))

;; Vertico repeat last command
(use-package vertico-repeat
  :hook (minibuffer-setup . vertico-repeat-save)
  :commands (vertico-repeat-last))

;; Configure directory extension
(use-package vertico-directory
  :after vertico
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
         ("DEL" . vertico-directory-delete-char)
         ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))
;; Useful macro to wrap functions in for testing
;; See https://stackoverflow.com/q/23622296
(defmacro measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (message "
;; ======================================================
;; *Elapsed time: %.06f*
;; ======================================================"
              (float-time (time-since time)))))

;; A few more useful configurations...
(measure-time
 (message "*Loading further vertico completion settings...*")
 ;; Add prompt indicator to `completing-read-multiple'.
 (defun crm-indicator (args)
   (cons (concat "[CRM] " (car args)) (cdr args)))
 (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

 ;; Grow and shrink minibuffer
 (setq resize-mini-windows t)

 ;; Do not allow the cursor in the minibuffer prompt
 (setq minibuffer-prompt-properties
       '(read-only t cursor-intangible t face minibuffer-prompt))
 (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

 ;; Enable recursive minibuffers
 (setf enable-recursive-minibuffers t))

;; Persist history over Emacs restarts with savehist mode. Vertico sorts by history position.
;; Savehist is set up in lem-setup-settings.el

;;;;; Ordering
;; Setup for vertico
;; Use the `orderless' completion style.
;; Enable `partial-completion' for files to allow path expansion.
;; You may prefer to use `initials' instead of `partial-completion'.
(use-package orderless
  :straight t
  :init
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles . (partial-completion))))))

(use-package pdf-tools
    :defer t
:commands (pdf-view-mode pdf-tools-install)
 :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
 :magic ("%PDF" . pdf-view-mode)     :config
	(pdf-tools-install)
	(setq-default pdf-view-display-size 'fit-page)
    :bind (:map pdf-view-mode-map
	  ("\\" . hydra-pdftools/body)
	  ("<s-spc>" .  pdf-view-scroll-down-or-next-page)
	  ("g"  . pdf-view-first-page)
	  ("G"  . pdf-view-last-page)
	  ("l"  . image-forward-hscroll)
	  ("h"  . image-backward-hscroll)
	  ("j"  . pdf-view-next-page)
	  ("k"  . pdf-view-previous-page)
	  ("e"  . pdf-view-goto-page)
	  ("u"  . pdf-view-revert-buffer)
	  ("al" . pdf-annot-list-annotations)
	  ("ad" . pdf-annot-delete)
	  ("aa" . pdf-annot-attachment-dired)
	  ("am" . pdf-annot-add-markup-annotation)
	  ("at" . pdf-annot-add-text-annotation)
	  ("y"  . pdf-view-kill-ring-save)
	  ("i"  . pdf-misc-display-metadata)
	  ("s"  . pdf-occur)
	  ("b"  . pdf-view-set-slice-from-bounding-box)
	  ("r"  . pdf-view-reset-slice)))

    ;; (use-package org-pdfview
    ;;     :config 
    ;;             (add-to-list 'org-file-apps
    ;;             '("\\.pdf\\'" . (lambda (file link)
    ;;             (org-pdfview-open link)))))

(straight-use-package 'all-the-icons)
(use-package lambda-line
  :straight (:type git :host github :repo "lambda-emacs/lambda-line") 
  :custom
  (lambda-line-icon-time t) ;; requires ClockFace font (see below)
  (lambda-line-clockface-update-fontset "ClockFaceRect") ;; set clock icon
  (lambda-line-position 'top) ;; Set position of status-line 
  (lambda-line-abbrev t) ;; abbreviate major modes
  (lambda-line-hspace "  ")  ;; add some cushion
  (lambda-line-prefix t) ;; use a prefix symbol
  (lambda-line-prefix-padding nil) ;; no extra space for prefix 
  (lambda-line-status-invert nil)  ;; no invert colors
  (lambda-line-gui-ro-symbol  " ⨂") ;; symbols
  (lambda-line-gui-mod-symbol " ⬤") 
  (lambda-line-gui-rw-symbol  " ◯") 
  (lambda-line-space-top +.50)  ;; padding on top and bottom of line
  (lambda-line-space-bottom -.50)
  (lambda-line-symbol-position 0.1) ;; adjust the vertical placement of symbol
  :config
  ;; activate lambda-line 
  (lambda-line-mode) 
  ;; set divider line in footer
  (when (eq lambda-line-position 'top)
    (setq-default mode-line-format nil ) ;;(list "%_"))
    (setq mode-line-format nil))) ;;(list "%_"))))

(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)

(defun lem--workspace-setup ()
  "Set up workspace at startup."
  ;; Add *Messages* and *splash* to Tab \`Home\'
  (progn
    (tab-bar-rename-tab "Home")
    (when (get-buffer "*Messages*")
      (set-frame-parameter nil
                           'buffer-list
                           (cons (get-buffer "*Messages*")
                                 (frame-parameter nil 'buffer-list))))
    (when (get-buffer "*splash*")
      (set-frame-parameter nil
                           'buffer-list
                           (cons (get-buffer "*splash*")
                                 (frame-parameter nil 'buffer-list))))))

(add-hook 'after-init-hook #'lem--workspace-setup)
(defun lem-go-home ()
  (interactive)
  (tab-bar-switch-to-tab "Home"))
;;;;; Open Project in New Workspace
(defun lem-open-existing-project-and-workspace ()
  "Open a project as its own workspace"
  (interactive)
  (progn
    (tab-bar-new-tab)
    (call-interactively 'project-switch-project-open-file)
    (tab-bar-rename-tab (tabspaces--name-tab-by-project-or-default))
    (project-magit-dir)))

;;;;; Open Agenda as Workspace
(defun lem-open-agenda-in-workspace ()
  "Open agenda in its own workspace"
  (interactive)
  (if (member "Agenda" (tabspaces--list-tabspaces))
      (progn
        (tab-bar-switch-to-tab "Agenda")
        (switch-to-buffer "*Org Agenda*")
        (org-agenda-redo)
        (delete-other-windows))
    (progn
      (tab-bar-new-tab)
      (tab-bar-rename-tab "Agenda")
      (require 'org)
      (org-agenda-list))))

; (load-theme 'nord t)
(defun operandi-with-my-settings ()
  (interactive)
(setq	    modus-themes-syntax nil) ;; '(faint alt-syntax ))
(load-theme 'modus-operandi t)
)
(defun vivendi-with-my-settings ()
  (interactive)
	(setq modus-themes-org-blocks nil
	      modus-themes-org-blocks 'grayscale
	      modus-themes-fringes 'nil
	      ;; modus-themes-variable-pitch-ui t
	      modus-themes-prompts '(bold background)
	      modus-themes-completions 'opinionated
	      modus-themes-subtle-line-numbers t
	      modus-themes-italic-constructs t
	      modus-themes-syntax '(faint alt-syntax )
	      ;; modus-themes-variable-pitch-headings t
	      ;; modus-themes-variable-ui-headings t
	      modus-themes-subtle-line-numbers t
	      modus-themes-tabs-accented t
	      modus-themes-paren-match '(bold intense)
	      modus-themes-bold-constructs nil
	      ;; modus-themes-hl-line '(accented intense)
	      ;; modus-themes-region '(intense)
	      ;; modus-themes-mode-line '(accented 3d borderless (padding. 22) (height . 0.9))
	      modus-themes-org-agenda
	      '((header-block . (variable-pitch scale-title))
		(header-date . (bold-today grayscale scale))
		(scheduled . rainbow)
		(habit . traffic-light-deuteranopia))
      modus-themes-headings
	  '((1 . (background overline variable-pitch 1.28))
	    (2 . (variable-pitch 1.22))
	    (3 . (semibold 1.17))
	    (4 . (1.14))
	    (t . (monochrome)))
  )

(load-theme 'modus-vivendi t)
)
(vivendi-with-my-settings)

(defun normal-font ()
(interactive)
(set-face-attribute 'default nil :font "Iosevka " :height 100)
(set-face-attribute 'fixed-pitch nil :font "Iosevka" :height 100)
)
(defun big-font ()
(interactive)
(set-face-attribute 'default nil :font "Iosevka " :height 100)
(set-face-attribute 'fixed-pitch nil :font "Iosevka" :height 100)
)
(normal-font)

(setq 
;; org-ellipsis "▼  " ;;⤵ 
	  ;; org-startup-indented t
	  org-src-tab-acts-natively t
	  org-hide-emphasis-markers t
	  org-fontify-done-headline t
	  org-hide-leading-stars t
	  org-pretty-entities t
	  org-odd-levels-only t
	  ) 
  ;;  (add-hook 'org-mode-hook #'org-shifttab)
    ;; (add-hook 'org-mode-hook #'variable-pitch-mode)
    ;; (setq org-bullets-bullet-list '(
    ;; (add-hook 'org-mode-hook (lambda () (local-set-key (kbd "
  (setq org-src-fontify-natively t
org-startup-folded t
	org-edit-src-content-indentation 0)

(defun my-org-confirm-babel-evaluate (lang body)
  (not (string= lang "haskell"))) ;; don't ask for haskell
(setq org-confirm-babel-evaluate #'my-org-confirm-babel-evaluate)
(setq org-confirm-babel-evaluate 'nil)

(setq org-babel-load-languages '((emacs-lisp . t) (shell . t) (dot . t) (haskell  . t) (nix . t) ))

(defun insert-org-haskell ()
    "Inserts code haskell block in org"
    (interactive)
    (insert "#+BEGIN_SRC haskell")
    (newline)
    (insert ":{")
    (newline)
    (insert ":}")
    (newline)
    (insert "#+END_SRC ")
    (previous-line 2)
    (newline)
)

(defun emux/getBuffer()
  (interactive)
  (setq buff (read-buffer-to-switch "Raise window"))
  (display-buffer buff '(display-buffer-reuse-window . ())))

(defun emux/go-to-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Go to window"))
  (select-window (get-buffer-window buff)))

(defun emux/get-and-to-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Raise & Go to window"))
  (display-buffer buff '(display-buffer-reuse-window . ()))
  (select-window (get-buffer-window buff)))

(defun emux/close-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Close window"))
  (delete-window (get-buffer-window buff)))

(defun emux/close-help ()
  (interactive)
  (setq buff "*Help*") 
  (delete-window (get-buffer-window buff)))

(defun emux/close-scratch ()
  (interactive)
  (setq buff "*scratch*") 
  (delete-window (get-buffer-window buff)))

(require 'window)
(setq display-buffer-alist
      `(
	("\\*vterm*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . 0)
	 )
	("\\*shell*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . -1)
	 )
	("\\`\\*Async Shell Command\\*\\'"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . right)
	 (slot . 0)
	 )
	("\\*scratch*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top)
	 (slot . 0)
	 )
	;; ("*"
	;;  (display-buffer-in-side-window)
	;;  (mode . (dired-mode))
	;;  (dedicated . t)
	;;  (window-height . 0.23)
	;;  (side . bottom)
	;;  (slot . 0)
	;;  )
	("*Help*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom) 
	 (slot . 1)
	 )
	("*Occur*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top) 
	 (slot . -1)
	 )
	("*grep*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top) 
	 (slot . 1)
	 )
	("\\*haskell*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . -1)
	 )
	)
      )

(setq org-agenda-files '("~/tasks/timetable.org")
      org-agenda-span  'week
      org-todo-keywords '((sequence "NEXT(n)" "TODO(t)" "WAITING(w)" "SOMEDAY(s)" "|" "DONE(d)" "CANCELLED (c)")) 
      org-log-done 'time
      org-log-done 'note
      )

(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-log-into-drawer '("LOGBOOK"))

(column-number-mode 1)
(global-display-line-numbers-mode 0)
(setq display-line-numbers-type 'visual)
(dolist (mode '( org-mode-hook
		 prog-mode-hook
		 dired-mode-hook

		 ;; term-mode-hook
		 ;; vterm-mode-hook
		 ;; shell-mode-hook
		 ;; dired-mode-hook
		 ;; eshell-mode-hook
		 ))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(setq org-capture-templates
'(
("j" "Journal" entry (file+datetree "~/journal.org")
"* %?\n %i\n %a")
("r" "Reading Journal" entry (file+datetree "~/ reading.org")
"* %?\n %i\n %a")
("b" "Templates"
)
))
