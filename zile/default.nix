{pkgs, ... }:
{
  home.packages = with pkgs; [
    zile
    xournal
];
}
