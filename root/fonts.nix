{pkgs,...}:{
fonts.fonts = with pkgs; [
    source-code-pro
    emacs-all-the-icons-fonts
    fira-code
    victor-mono
    roboto-mono
    inconsolata-nerdfont
    (nerdfonts.override { fonts = [ "RobotoMono" ]; }) # "CascadiaCode" "Hasklig" "JetBrainsMono" "Mononoki" "SourceCodePro" ]; })
    ];
    }
