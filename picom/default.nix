{ ... }: {
  services.picom = {
    enable = true;
    activeOpacity = 1.0;
    backend = "xr_glx_hybrid";
    settings = {
      blur = {
        method = "gaussian";
        size = 10;
        deviation = 5.0;
      };
    };
  };
}
