{ config, pkgs, ... }: {
  programs.kakoune = {
    enable = true;
    plugins = with pkgs.kakounePlugins;
      [
        #     kakoune-easymotion
        #   quickscope-kak
        powerline-kak
      ];
    extraConfig = ''
          	powerline-start
      set-option global modelinefmt '%val{bufname} %val{cursor_line}:%val{cursor_char_column} {{context_info}} {{mode_info}}'
    '';
#      colorScheme operandi
    config = {
      #	    colorScheme = "operandi";
      ui = { statusLine = "top";
      assistant = "dilbert";
      };
      numberLines = {
        enable = true;
        highlightCursor = true;
        relative = true;
      };
    };
  };
}
