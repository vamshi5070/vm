{pkgs,...}:
{
  home.packages = with pkgs; [entr tree];
  programs.zoxide.enable = true;
  programs.fzf.enable = true;
  programs.fzf.enableFishIntegration = true;
}
