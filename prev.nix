          pkgs = import inputs.nixpkgs {
            system = "aarch64-linux";
            config.allowUnfree = true;
          };
          system = "aarch64-linux";
          homeDirectory = "/home/vamshi";
          username = "vamshi";
          configuration = { dmenu, config, lib, pkgs, ... }: {
            nixpkgs.overlays = with inputs;
              [
                  emacs-overlay.overlay
                #  nur.overlay
                #  kmonad.overlay
                neovim-nightly-overlay.overlay
              ];
            nixpkgs.config = { 
              allowUnfree = true;
              allowUnsupportedSystem = true;
            };
            # This value determines the Home Manager release that your
            # configuration is compatible with. This helps avoid breakage
            # when a new Home Manager release introduces backwards
            # incompatible changes.
            #
            # You can update Home Manager without changing this value. See
            # the Home Manager release notes for a list of state version
            # changes in each release.
            # home.stateVersion = "21.05";
            # home.keyboard = null;
            # Let Home Manager install and manage itself.
            programs = { home-manager.enable = true; };
            imports =
              [ ./latex ./zathura ./emacs # ./stack
                ./htop ./git ./fish ./neovim
./kakoune		./kitty
                ./direnv ./starship ./browsers ./utilities ./unclutter ./helix ./vscode ];
          };
        };
      };
    };
}



    extraPackages = epkgs:
      with epkgs; [
        org
        vterm
        multi-term
        elm-mode
        haskell-mode
        nix-mode
        rust-mode
        purescript-mode
        envrc
        magit
        # mini-modeline
        org-auto-tangle
        # popwin
        popper
        # shackle
        # dired-sidebar
        # org-babel
        vertico
        mini-frame
        vertico-posframe
        orderless
        consult
        embark
        marginalia
        which-key
        # editor
        # evil
        # evil-collection
        # general
        # evil-snipe
        # meow
        # modeline
        # mini-modeline
      #   doom-modeline
      #   doom-themes
      #   kaolin-themes
        # nano-theme
        # nano-modeline
        # nano-agenda
 ];
    #Pkgs
 
