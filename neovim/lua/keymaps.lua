M = {}

local opts = { noremap = true, silent = false}

local term_opts = {silent = false}

--Shorten function name
local keymap = vim.api.nvim_set_keymap

keymap("","<Space>","<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--Modes
-- normal mode = n
-- insert mode = i
-- visual mode = v
-- visual block mode = x
-- term mode  = t
-- command mode = c

-- Nvim tree
keymap("n", "<leader>e", ":NvimTreeToggle<CR>", opts)

-- Telescope
keymap("n", "<leader>ff", ":Telescope fd<CR>", opts)
keymap("n", "<leader>ft", ":Telescope live_grep<CR>", opts)
keymap("n", "<leader>bb", ":Telescope buffers<CR>", opts)
keymap("n", "<leader>fs", ":w<CR>", opts)

-- windows
keymap("n", "<leader>ws", ":", opts)
