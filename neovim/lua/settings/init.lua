local options = {
	expandtab = true,
        relativenumber = true,
        wrap = false,
        syntax = on,
	clipboard = "unnamedplus",
        cmdheight= 1,
        guifont= "Iosevka:h7",
        termguicolors = true
 --       colorscheme = codedark
}

local opt = vim.opt
-- local g = vim.g

-- g.mapleader = ' '

require'hop'.setup()

vim.cmd [[
        map ; :
]]

--opt.
--opt.
--opt.

for key,value in pairs(options) do
	vim.opt[key] = value
end

require("bufferline").setup{}
require("toggleterm").setup{
        -- size = 20,
        -- open
}
