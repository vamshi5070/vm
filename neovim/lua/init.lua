--require("bufferline").setup{}
--require("defaults")
local options = {
        relativenumber = true,
        syntax = off,
        guifont= "Monospace:h12"
}

for key,value in pairs(options) do
	vim.opt[key] = value
end
