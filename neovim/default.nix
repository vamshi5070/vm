{ pkgs, lib,  ... }: {
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    package = pkgs.neovim-nightly;
    plugins = with pkgs.vimPlugins; [
  yankring
  vim-nix
  { plugin = vim-startify;
    config = "let g:startify_change_to_vcs_root = 0";
  }
  vim-airline
  neorg
    #plugin = onedarkpro-nvim;
    #config = "colorscheme onedarkpro";
];
  extraConfig = ''
          luafile ~/vm/neovim/lua/init.lua
    '';
  };

#  home.file.".config/alacritty/alacritty.yml".source = ../alacritty.yml;
 home.packages = with pkgs; [
    # nodejs
    # ranger
    # scrot
    neovide
    gnvim
    neovim-qt
  ];
}
