{ pkgs, config, lib,  ... }:{
  home.packages = with pkgs; [xorg.libxcvt];
  programs.fish = {
  	enable = true;
    #set hook $(printf "{\"hook\": \"SSH\", \"value\": {\"socket_path\": \"~/.ssh/166809860517341\", \"user\": \"%s\", \"machine\": \"%s\", \"remote_shell\": \"%s\"}}" $(whoami) $(hostname) ${SHELL##*/} | command od -An -v -tx1 | command tr -d " \n")

    interactiveShellInit = "
		### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
		set PATH $PATH /nix/store/nlgwv0h3pqrng56gjfzi4v5bpalwjn2y-system-path/bin
		set fish_color_normal brcyan
		set fish_color_autosuggestion '#7d7d7d'
		set fish_color_command brcyan
		set fish_color_error '#ff6c6b'
    set fish_color_param brcyan ";
		shellInit = "
    export PATH=\"$HOME/.emacs.d/bin:$PATH\"
    set fish_greeting";
		shellAliases = {
			conf = "sudo nvim /etc/nixos/configuration.nix";
			# homeConf =  "nvim ~/.config/nixpkgs/home.nix";
			#up = "sudo nixos-rebuild switch"; # && notify-send -u low \"Successfully updated\" ";
			up-boot = "sudo nixos-rebuild boot --install-bootloader";
			del = "sudo nix-collect-garbage -d";
      # server = "php -S localhost:4000";
      # bright = "systemctl --user stop redshift.service";
      # dark = "systemctl --user restart redshift.service";
      addRoot = "cp -r /etc/nixos/* ~/vm/root/";
      up= "sudo nixos-rebuild switch";
      wifi = "systemctl start wpa_supplicant-wlp2s0.service";
      c = "clear";
      e = "exit";
      homeUp = "home-manager switch --flake \"/home/vamshi/vm#cosmos\"" ;
      nes   = "nix-instantiate --eval --strict";
      ne   = "nix-instantiate --eval ";
      #"nix-instantiate";
      # ls = "exa";
      # ll = "exa -l -g --icons";
		};
  };
  # xdg.configFile."fish/functions/fish_prompt.fish".text = customPlugins.prompt;
}
