{ pkgs, ... }: {
  programs.helix = {
    enable = true;
    package = pkgs.helix;
    settings = {
      theme = "night_owl";
      #     lsp.display-messages = true;
      editor = {
        line-number = "relative";
        cursor-shape = {
          insert = "bar";
          normal = "block";
          select = "underline";
        };
      };
      keys.normal = {
        space.space = "file_picker";
        space.w = ":w";
        space.q = ":q";
      };
    };
  };
}
