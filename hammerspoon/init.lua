-- install hammerspoon cli
local brewPrefixOutput, _, _, _ = hs.execute("brew --prefix", true)
local brewPrefix = string.gsub(brewPrefixOutput, "%s+", "")
require("hs.ipc")
local ipc = hs.ipc.cliInstall(brewPrefix)
print(string.format("ipc: %s", ipc))

-- Make all our animations really fast
hs.window.animationDuration = 0

-- Load SpoonInstall, so we can easily load our other Spoons
hs.loadSpoon("SpoonInstall")
spoon.SpoonInstall.use_syncinstall = true
Install = spoon.SpoonInstall

spoon.SpoonInstall:andUse("AppLauncher", {
--   modifiers =  { "alt" ,"cmd"  },
   hotkeys = {
     a = "Adobe After Effects 2022",
     b = "Blender",
     c = "Calendar",
     d = "Discord",
     e = "Emacs",
     f = "Firefox",
     n = "Notes",
     p = "1Password 7",
     r = "Reeder",
     t = "Terminal",
     u = "UTM",
     p = "Preview",
     s = "Safari",
     z = "Zoom.us",
   }
})

-- hs.notify.show("rgv" ,"","")
-- Draw pretty rounded corners on all screens
Install:andUse("RoundedCorners", {
    start = true,
    config = {
        radius = 12,
    },
})
hs.hotkey.bind({"cmd", "alt", "ctrl"}, "H", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()

  f.x = f.x - 10
  win:setFrame(f)
end)
hs.hotkey.bind({"cmd", "alt", "ctrl"}, "W", function()
--  hs.reload()
  hs.alert.show("Hello World!")
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "Left", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x
  f.y = max.y
  f.w = max.w / 2
  f.h = max.h
  win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "Right", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x + (max.w / 2)
  f.y = max.y
  f.w = max.w / 2
  f.h = max.h
  win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "Up", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x -- + (max.w / 2)
  f.y = max.y
  f.w = max.w -- / 2
  f.h = max.h / 2
  win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "Down", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x -- + (max.w / 2)
  f.y = max.y + (max.h / 2)
  f.w = max.w -- / 2
  f.h = max.h / 2
  win:setFrame(f)
end)


hs.hotkey.bind({"cmd", "alt", "Down"}, "Left", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x -- + (max.w / 2)
  f.y = max.y + (max.h / 2)
  f.w = max.w  / 2
  f.h = max.h / 2
  win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "f", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x -- + (max.w / 2)
  f.y = max.y -- + (max.h / 2)
  f.w = max.w -- / 2
  f.h = max.h -- / 2
  win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "Up"}, "Left", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x -- + (max.w / 2)
  f.y = max.y -- + (max.h / 2)
  f.w = max.w  / 2
  f.h = max.h / 2
  win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "Down"}, "Right", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x  + (max.w / 2)
  f.y = max.y  + (max.h / 2)
  f.w = max.w  / 2
  f.h = max.h / 2
  win:setFrame(f)
end)

hs.hotkey.bind({"cmd", "alt", "Up"}, "Right", function()
  local win = hs.window.focusedWindow()
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x  + (max.w / 2)
  f.y = max.y  --+ (max.h / 2)
  f.w = max.w  / 2
  f.h = max.h / 2
  win:setFrame(f)
end)

-- use as a replacement for ReloadConfiguration for now
hs.hotkey.bind({ "cmd", "ctrl", "shift" }, "r", function()
    hs.reload()
end)
-- TODO: why is this infinitely reloading?
-- Install:andUse("ReloadConfiguration", {
--     start = true,
--     hotKeys = {
--         reloadConfiguration = { { "cmd", "ctrl", "shift" }, "r" },
--     },
-- })

-- Install:andUse("Caffeine", {
--     start = true,
-- })
-- import keybindings for yabai
-- yabai = require("yabai")
-- caps2esc = require("caps2esc")
