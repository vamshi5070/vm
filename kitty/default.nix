{ config, pkgs, ... }: {
	programs.kitty = {
		enable = true;
		font = {
			name = "Victor Mono";
			# "Fira Code Nerd Font";
			size = 17;
		};
		theme = "Dracula";
	};
}
