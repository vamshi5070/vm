{ config, ... }:
let
  emanote = import (	builtins.fetchTarball {

    url = "https://github.com/srid/emanote/archive/master.tar.gz";
    sha256 = "sha256:0zfr6lcab9cy5qrympyrqga2z1v2fs7dqcfb2jfr5vmm3mlfywdj";
    # sha256 = "0php3z17b25fzayf30kk0nr195x19c6y1iiy9lnnmgna55hx3gkj"; #"01wc2npyjkgvdkm7byk35scgab9aby19z75rii46qf3py7c5h826";
    # "0000000000000000000000000000000000000000000000000000";
    # "000000000000000000000000000000000000000000";
  });
in {
  imports = [ emanote.homeManagerModule ];
  services.emanote = {
    enable = true;
    # host = "127.0.0.1"; # default listen address is 127.0.0.1
    # port = 7000;        # default http port is 7000
    notes = [
      "/home/user/notes"  # add as many layers as you like
    ];
    package = emanote.packages."aarch64-linux".default;
    #${builtins.currentSystem}.default;
  };
}
