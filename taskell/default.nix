{pkgs,...}:{
  home.packages = with pkgs; [
    xournal
    xournalpp
    taskell
    #        haskellPackages.clifm
  ];
}
