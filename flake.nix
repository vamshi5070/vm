{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
    #    nix-doom-emacs.url = "github:nix-community/nix-doom-emacs";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
  };
  outputs = inputs@{ self, nixpkgs, ... }:
  let

    inherit (inputs.nixpkgs.lib) attrValues makeOverridable optionalAttrs singleton;
    nixpkgsConfig = {
      #           config = { allowUnfree = true; };
      myOverlays = attrValues self.overlays ++ singleton (
        # Sub in x86 version of packages that don't build on Apple Silicon yet
        final: prev: (optionalAttrs (prev.stdenv.system == "aarch64-linux") {
          inherit (final.pkgs-x86)
          #                                                       idris2
          #                                                                   #nix-index
          #                                                                               niv
          purescript;
        })
      );
    };
    system = "aarch64-linux";
    myPkgs = import nixpkgs {
      inherit system;
      config = {
        allowUnfree = true;
        allowUnsupportedSystem = true;
      };
    };
    lib = nixpkgs.lib;
  in {
    homeConfigurations = {
      cosmos = inputs.home-manager.lib.homeManagerConfiguration {
        pkgs = myPkgs; # nixpkgs.legacyPackages.${system};
        modules = [
          # ./freeplane 
          #            ./zile
		      ./exercism
          ./kitty
          ./unzip
          #          ./krita
          ./nyxt
          # ./emanote
          ./just
          ./taskell
          # ./cabalStack
          # ./agda
          # ./exa
          #            ./zathura
          # ./taskell
          ./alacritty
          ./fish
          ./helix
          ./lapce
          ./ed
          ./vscode
          ./yi
          # ./termonad
          # ./commandLine
          # #./homeManager
          ./htop
          ./git
          ./emacs
          ./direnv
          ./nnn
          ./enter
          # ./browsers
          ./starship
          # ./command-not-found
          # ./ihp
          ./neovim
          ./kakoune
          # ./picom
          # ./tmux
          ./nix
          # ./unclutter
          #          ./utilities
          # ./latex
          ./wallpapers
          {
            nixpkgs.overlays = with inputs; [
              #
              emacs-overlay.overlay
              #  nur.overlay
              #  kmonad.overlay
              neovim-nightly-overlay.overlay
            ];

            programs.home-manager.enable = true;
            home = {
              username = "vamshi";
              homeDirectory = "/home/vamshi";
              stateVersion = "22.11";
            };
          }
        ];
      };
    };
  };
}

# Configuration for `nixpkgs`
#                                                                                                         );
#                                                                                                               # ++[
  #                                                                                                                     #        spacebar.overlay
  #                                                                                                                           # ];
  #                                                                                                                               };
  #
